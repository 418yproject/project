import React from "react";
import { app } from './App';
import 'firebase/compat/firestore';
import firebase from 'firebase/compat/app';

import API_KEY from './env';

import { getFirestore, getDoc, doc, setDoc } from "firebase/firestore";

function ApiTest(value, option) {
    console.log("Called with: ", value.asin, option);
    if (option === "search") {
        fetch("https://amazon-price1.p.rapidapi.com/search?keywords=" + value.asin + "&marketplace=US", {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "amazon-price1.p.rapidapi.com",
                "x-rapidapi-key": API_KEY
            }
        }
        )
            .then(response => response.json())
            .then(data => {
                const db = getFirestore(app);
                //add to items collection so user can decide then if they want to follow or not 
                setDoc(doc(db, "items/", value), {
                    name: data[0]['title'],
                    imagelink: data[0]['imageUrl'],
                    amazonlink: data[0]["detailPageURL"],
                    price: data[0]['price'],
                    asin: data[0]["ASIN"]
                });

            })
            .catch(err => {
                console.error(err);
            });
    } else if (option == "store") {
        //get most recent asin search from db 
        const db = getFirestore(app);
        const docRef = doc(db, "searches", "asins");
        const docSnap = getDoc(docRef);
        
        docSnap.then(data => {
            var asin = data.get('value');
            fetch(`https://amazon-price1.p.rapidapi.com/search?keywords=${asin}&marketplace=US`, {
                "method": "GET",
                "headers": {
                    "x-rapidapi-host": "amazon-price1.p.rapidapi.com",
                    "x-rapidapi-key": API_KEY
                }
            })
                .then(response => response.json())
                .then(apiData => {
                    var reference = firebase.firestore(app);
                    var time = new Date().toLocaleTimeString();
                    
                    //store asin to users/userid
                    const userRef = reference.collection('users').doc(value).collection('asins');
                    userRef.doc(asin).set({
                        price : apiData[0]['price']
                    });

                    // to get document id : const document = userRef.doc(asin).id;

                    //store product data to products collection 
                    const productRef = reference.collection('products').doc(asin);
                    productRef.set({
                        asin : apiData[0]['ASIN'],
                        title : apiData[0]['title'],
                        image : apiData[0]['imageUrl']
                    });
                    //price sub collection includes price and timestamp of response 
                    productRef.collection('price').doc('data').set({
                        price : apiData[0]['price'], 
                        timestamp : time
                    });

                })
                .catch(err => {
                    console.error(err);
                });

        });

    }

    return (

        <div className="App">
            <h1>Hello World!</h1>
        </div>
    );
}

export default ApiTest;
