import React from 'react'
import './home.css'

import ApiTest from '../ApiTest'
import ArcGlobe from '../components/ArcGlobe';

export default function Home() {
    return (
        <div class="primary wrapper">
            <div class="container">
                <h1>Follow Your</h1>
                <h1>Favorite Items</h1>
                <h3>Find the perfect time to buy</h3>
            </div>
            <div id="globe1">
                <div id="globe">
                    <ArcGlobe/>
                </div>
            </div>
        </div>
    )
}