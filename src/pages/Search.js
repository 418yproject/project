//Function returns a html form for searching


import { Button } from '@material-ui/core';
import React from 'react'
import { useState } from 'react';
import ApiTest from '../ApiTest';
import AddItemToItems from '../components/AddItemToItems';
import AddItemToUser from '../components/AddItemToUser'
import CheckIfItemExists from '../components/CheckIfItemExists';
import TrackItem from '../components/TrackItem';
import { Typography } from '@material-ui/core';

import './search.css';

export default function Search() {
  const [asin, setAsin] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    //Store every asin into user collection 
    ApiTest(asin, "search");
    console.log("Item Successfully added to user!");
  }
  
    return (
      <div id="search-wrapper">
      <div class="bg"></div>
      <div class="bg bg2"></div>
      <div class="bg bg3"></div>
      <div class="content">
        <div class="title">
          <Typography variant="h3">
                Enter the Asin
          </Typography>
        </div>
        <form onSubmit={handleSubmit}>
          <input 
              type="text"
              className="inputBox"
              value={asin}
              onChange={(e) => setAsin(e.target.value)}
            />
          <input type="submit" />
        </form>
        </div>
      </div>
    )
}

