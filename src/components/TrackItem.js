import React from 'react'

import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';
import { app, auth } from '../App';
import { getFirestore, getDoc, doc, setDoc } from "firebase/firestore";
import Notify from '../Notification';

var firebaseConfig = {
    // Will's firestore
    apiKey: "AIzaSyDJtn7YR8Y2bttb50CZ72K3Rw5aF-hW4sg",
    authDomain: "pricefollow-cc0f3.firebaseapp.com",
    projectId: "pricefollow-cc0f3",
    storageBucket: "pricefollow-cc0f3.appspot.com",
    messagingSenderId: "650447652443",
    appId: "1:650447652443:web:5d2706685bb1c99d6be17e"

    //Gordon's firestore
    // apiKey: "AIzaSyDqXHk4u854ARKnYpBxy3Z9qV4y_3Z9CIQ",
    // authDomain: "y-project-326901.firebaseapp.com",
    // projectId: "y-project-326901",
    // storageBucket: "y-project-326901.appspot.com",
    // messagingSenderId: "613471128815",
    // appId: "1:613471128815:web:8ba988d6f87b295ea41f1a",
    // measurementId: "G-ZRHW4M9ME8"
};

firebase.initializeApp(firebaseConfig);

//This function will check for price changes every 24 hours 
export default function TrackItem(asin) {
    fetch(`https://amazon-price1.p.rapidapi.com/search?keywords=${asin}&marketplace=US`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "amazon-price1.p.rapidapi.com",
            "x-rapidapi-key": "53ba664331msh4f424dce079498ap1fec96jsnc0f0edeec434"
        }
    }
    )
        .then(response => response.json())
        .then(apiData => {
            //api responses
            var strPrice = (String)(apiData[0]['price']);
            var itemName = apiData[0]['title'];
            var asin = apiData[0]['asin'];

            //Accessing data in firestore
            const db = getFirestore(app);
            const docRef = doc(db, "users/" + auth.currentUser.uid + "/followedItems/" + asin); //Or whatever the path to the asin is
            const docSnap = getDoc(docRef);

            docSnap.then(dbData => {
                var strDbPrice = (String)(dbData.get('price'));
                var currentPrice = parseFloat(strPrice.substr(1, strPrice.length - 1));
                var dbPrice = parseFloat(strDbPrice.substr(1, strDbPrice.length - 1));

                //if current asin price < db asin price -> notify(user)
                if (currentPrice < dbPrice) {

                    Notify(itemName, currentPrice);
                    clearInterval(TrackItem);
                    //console.log("notify succcess");
                }
            });

        })
        .catch(err => {
            console.error(err);
        });


    //This will call TrackItem function every day (86400 seconds = 1 day)
    setInterval(TrackItem, 86400);

}
