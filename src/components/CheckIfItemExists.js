//This function checks if the item exists in the collection holding all items.

import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';
import { useCollectionData} from 'react-firebase-hooks/firestore';

const firestore = firebase.firestore();

export default function CheckIfItemExists(asin) {

    //Gets all of the items from the database.
    const itemsRef = firestore.collection('items');
    const query = itemsRef.orderBy('name');
    const [items] = useCollectionData(query, { idField: 'id' });

    //Check if the asin of any of the items in the database is the same as the asin passed in.
    for(var i = 0; i < items.length; i++) {
        if(items[i].asin === asin) {
            return items[i];
        }
    }
    return false;
}
