import Globe from 'react-globe.gl';
import { useEffect, useRef } from 'react';


const N = 20;
const arcsData = [...Array(N).keys()].map(() => ({
  startLat: (Math.random() - 0.5) * 180,
  startLng: (Math.random() - 0.5) * 360,
  endLat: (Math.random() - 0.5) * 180,
  endLng: (Math.random() - 0.5) * 360,
  color: [['red', 'white', 'blue'][Math.round(Math.random() * 3)], ['red', 'white', 'blue'][Math.round(Math.random() * 3)]]
}));

function ArcGlobe() {
    const globeEl = useRef();

    useEffect(() => {
      globeEl.current.controls().enableZoom = false;
      globeEl.current.controls().autoRotate = true;
      globeEl.current.controls().autoRotateSpeed = 0.7;
    }, []);

    return (
    <Globe
        ref={globeEl}
        width="1120"
        height="1500"
        globeImageUrl="https://upload.wikimedia.org/wikipedia/commons/e/ec/World_map_%28blue_dots%29.svg"
        backgroundColor="#141414"
        atmosphereColor="#0048AB"
        arcsData={arcsData}
        arcColor={'color'}
        arcDashLength={() => Math.random()}
        arcDashGap={() => Math.random()}
        arcDashAnimateTime={() => Math.random() * 4000 + 500}
        />
    );
}
export default ArcGlobe;