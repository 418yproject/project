//This function will take in a json object representing an item, and add it to the database under "users/userid/items"

import React from 'react'

import { app } from '../App';
import 'firebase/compat/firestore';

import { getFirestore, doc, setDoc } from "firebase/firestore";

export default function AddItemToItems(item) {
    const db = getFirestore(app);
    //add user info to 'users' collection using google id as path 
    setDoc(doc(db, "items"), {
        //price : ApiPriceCheck(item.asin),
        name : item.name,
        imagelink : item.imagelink,
        amazonlink : item.amazonlink,
        description : item.description,
        asin : item.asin
      }, { merge: true });
  

    return (
        <>
        console.log("item added to items");
        </>
    )
}
