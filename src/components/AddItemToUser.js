//This function will take in a json object representing an item, and add it to the database under "users/userid/items"

import React from 'react'

import { app, auth } from '../App';
import 'firebase/compat/firestore';
import { getFirestore, doc, setDoc } from "firebase/firestore";
import TrackItem from './TrackItem';


export default function AddItemToUser(item) {
    console.log("AddItemToUser: item: ", item.itm);
    console.log("AddItemToUser: item: ", item.itm.name);

    let product = item.itm;

    const db = getFirestore(app);
    const user_id = auth.currentUser.uid;

    //add user info to 'users' collection using google id as path 
    setDoc(doc(db, "users", user_id, "followedItems", product.asin), {
        name : product.name,
        imagelink : product.imagelink,
        amazonlink : product.amazonlink,
        asin : product.asin
      }, { merge: true });
  
    setTimeout(TrackItem(product.asin), 5000);

    return (
        <>
        console.log("item added to user");
        </>
    )
}
