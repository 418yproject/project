import { getFirestore, collection, getDoc, doc } from "firebase/firestore";
import { app, auth } from './App';
import axios from 'axios'
//import { useAnimatedState } from "framer-motion/types/animation/use-animated-state";

// open -n -a "Google Chrome" --args --user-data-dir=/tmp/temp_chrome_user_data_dir http://localhost:3000/ --disable-web-security

//Email the user 
function Notify(itemName, price) {
    var user = auth.currentUser;
    //API only allows 50 free calls a month, ~45 left
    console.log("notifying");
    const options = {
        method: 'POST',
        url: 'https://email-sender1.p.rapidapi.com/',
        params: {
            txt_msg: 'Hello, ' + user.displayName + ', one of your followed products has lowered its price to ' + price + '!!!',
            to: user.email,
            from: 'No-Reply-Product-Tracker-Notifications',
            subject: 'Hurry! Your Product, ' + itemName + ', is Waiting !',
            bcc: 'bcc-mail@gmail.com',
            reply_to: 'reply-to@gmail.com',
            //html_msg: '<html><body><b>test of the body</b></body></html>',
            cc: 'cc-mail@gmail.com'
        },
        headers: {
            'content-type': 'application/json',
            'x-rapidapi-host': 'email-sender1.p.rapidapi.com',
            'x-rapidapi-key': API_KEY
        },
        data: { key1: '1', key2: '2' } //I don't know what this does
    };


}
export default Notify;
